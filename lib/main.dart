import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'package:zoysii/ui/themes.dart';
import 'package:zoysii/ui/screens/home/home_page.dart';
import 'package:zoysii/ui/screens/rules/rules_page.dart';
import 'package:zoysii/util/local_data_controller.dart';

const List<Locale> supportedLocales = [
  Locale('en'),
  Locale('it'),
];

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await loadStoredData();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(Zoysii());
}

class Zoysii extends StatefulWidget {
  static _ZoysiiState of(BuildContext context) =>
      context.findAncestorStateOfType();

  @override
  _ZoysiiState createState() => _ZoysiiState();
}

class _ZoysiiState extends State<Zoysii> {
  @override
  Widget build(BuildContext context) {
    return I18n(
      initialLocale: settings.locale,
      child: MaterialApp(
        title: 'Zoysii',
        theme: lightTheme,
        darkTheme: darkTheme,
        themeMode: settings.theme,
        home: settings.firstRun ? RulesPage(HomePage()) : HomePage(),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: supportedLocales,
      ),
    );
  }
}

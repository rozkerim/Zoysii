import 'package:zoysii/models/board.dart';
import 'package:zoysii/util/random.dart';

// i18n: 'right'.i18n, 'left'.i18n, 'up'.i18n, 'down'.i18n
enum Direction { right, left, up, down }

extension ExtDirection on Direction {
  // Get a random direction based on board status and player position
  static Direction random(int position, int targetPosition, Board board) {
    Direction direction;
    for (var j = position; j % board.width != 0; j++) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.right;
      }
    }
    for (var j = position; (j + 1) % board.width != 0; j--) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.left;
      }
    }
    for (var j = position; j < board.grid.length; j += board.width) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.down;
      }
    }
    for (var j = position; j >= 0; j -= board.width) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.up;
      }
    }
    if (direction == null) {
      if (targetPosition < position) {
        direction = getRandom(0, 1) == 1 ? Direction.up : Direction.left;
      }
      if (targetPosition > position) {
        direction = getRandom(0, 1) == 1 ? Direction.right : Direction.down;
      }
      if (targetPosition == position) {
        if (position < (board.grid.length / 2).round()) {
          direction = getRandom(0, 1) == 1 ? Direction.right : Direction.down;
        } else {
          direction = getRandom(0, 1) == 1 ? Direction.up : Direction.left;
        }
      }
    }
    return direction;
  }
}

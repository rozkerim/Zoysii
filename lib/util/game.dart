import 'dart:async';
import 'package:flutter/foundation.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/board.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/models/level.dart';
import 'package:zoysii/models/player.dart';
import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/models/result.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/dialogs.dart';
import 'package:zoysii/util/direction.dart';
import 'package:zoysii/util/local_data_controller.dart';
import 'package:zoysii/util/random.dart';

class Game {
  // Game mode
  final GameMode mode;

  // Game page state
  final GamePageState _gamePage;

  // Total number of players
  final int _playerCount;

  // Milliseconds between players moves. 0 if the game is turn based
  final int gameSpeed;

  // Game board
  Board board;

  Game(
    this._gamePage,
    this.mode,
    this._playerCount, {
    this.gameSpeed = 0,
    this.board,
    int inputId,
    List<int> level,
  }) {
    loopRepeat = (inputId != null);
    selectedNumeralSystem =
        isLevelMode ? NumeralSystem.Arabic : settings.numeralSystem;
    init(newMatchId: inputId, levelId: level);
  }

  bool get isLevelMode => mode == GameMode.level;
  bool get isSingleMode => mode == GameMode.single;
  bool get isMultiMode => mode == GameMode.multi;

  // Match id
  // In levels mode id equals to: -(selectedLevel[0] * 7 + selectedLevel[1] + 1)
  int _id;
  int get id => _id;

  // Match status
  bool isPause = true;

  // Starting match date, saved as milliseconds since epoch
  int initDate;

  // List of players
  List<Player> players;

  // List of player sorted by points, moves and status
  List<Player> get ranking => List.from(players).cast<Player>()
    ..sort((a, b) {
      if (a.alive != b.alive) return a.alive ? -1 : 1;
      if (a.points != b.points) return b.points.compareTo(a.points);
      return a.moves.compareTo(b.moves);
    });

  // List of active players
  List<Player> get alivePlayers =>
      players.where((player) => player.alive).toList();

  // Player managed by user
  Player get playerOne => players.first;

  // Players managed by CPU in multi-player matches
  Player get playerTwo => players.length > 1 ? players[1] : null;
  Player get playerThree => players.length > 2 ? players[2] : null;
  Player get playerFour => players.length > 3 ? players[3] : null;

  // Best result for this grid size. Used only for single player match
  int _bestResult;

  // Result to beat
  int get resultToBeat {
    if (isMultiMode) {
      var _ranking = List.from(ranking).cast<Player>();
      if (_ranking.first == playerOne && _ranking.length > 1) {
        return _ranking.first.points - _ranking[1].points;
      } else {
        return playerOne.points - _ranking.first.points;
      }
    } else {
      return _bestResult;
    }
  }

  // Numeral system used during the match
  NumeralSystem selectedNumeralSystem;

  // Selected level (null if it is not level mode)
  Level level;

  // True if the match should be repeated again and again
  // Used for matches loaded from Ranking Page
  // It affects game dialogs
  bool loopRepeat;

  // Counts how many times restart button was pressed
  // Used only in levels mode
  int restartCount = 0;

  // Game timer used to move CPU players in multi-player mode
  Timer timer;

  // Index in alivePlayers of the player that can move (in turn based game)
  int _playerIndexTurn;
  Player get _activePlayer => alivePlayers[_playerIndexTurn];

  // Return true if player can move
  bool canMove(Player player) =>
      (!isPause && (gameSpeed != 0 || player == _activePlayer));

  // Initialize match. This method must be called before every match
  void init({int newMatchId, List<int> levelId}) {
    initDate = DateTime.now().millisecondsSinceEpoch;
    players = [
      Player(
          position: _getStartingPosition(0),
          color: playerColor[0],
          name: 'You'.i18n)
    ];
    board ??= Board();
    if (isLevelMode) {
      level = levels
          .firstWhere((l) => listEquals(l.levelId, levelId ?? level.levelId));
      playerOne.position = level.startingPosition ?? 0;
      board.createGrid(level);
    } else {
      timer?.cancel();
      _id = setRandom(newMatchId ?? id);
      board.createGrid();
      if (isMultiMode) {
        _playerIndexTurn = 0;
        players.addAll(List.generate(
          _playerCount - 1,
          (index) => Player(
            position: _getStartingPosition(index + 1),
            color: playerColor[index + 1],
            name: 'CPU ' + (index + 1).toString(),
            human: false,
          ),
        ));
        timer = Timer.periodic(
            Duration(milliseconds: (gameSpeed > 0 ? gameSpeed : 800)), (_) {
          alivePlayers.forEach((player) {
            if (!player.human && canMove(player)) match.movePlayer(player);
          });
        });
      } else {
        var _results = List.from(results.where((result) =>
            result.matchId >= 0 && result.sideLength == board.width));
        _results.sort((a, b) => b.points - a.points);
        _bestResult =
            _results.isNotEmpty ? _results.first.points : board.width * 24;
      }
    }
    isPause = false;
  }

  // Load next level or a new random match
  void next() {
    List<int> _level;
    if (!isLevelMode) {
      _id = -1;
    } else {
      _level = List.from(level.levelId);
      restartCount = 0;
      if (_level[1] < 6) {
        ++_level[1];
      } else if (_level[0] < layers.length - 1) {
        ++_level[0];
        _level[1] = 0;
      }
    }
    init(levelId: _level);
  }

  // Get starting position for each player
  int _getStartingPosition(int playerIndex) {
    switch (playerIndex) {
      case 1:
        return board.grid.length - 1;
      case 2:
        return board.width - 1;
      case 3:
        return board.grid.length - board.width;
      default:
        return 0;
    }
  }

  // Move player and update tiles value
  void movePlayer(Player player, [Direction direction]) {
    // Position increase
    int delta;

    // Update tiles until condition == true (ie until the end of the board)
    Function condition;

    switch (direction ??
        ExtDirection.random(player.position, playerOne.position, match.board)) {
      case Direction.right:
        delta = 1;
        condition = (index) => index % board.width != 0;
        break;
      case Direction.left:
        delta = -1;
        condition = (index) => (index + 1) % board.width != 0;
        break;
      case Direction.up:
        delta = -board.width;
        condition = (index) => index >= 0;
        break;
      case Direction.down:
        delta = board.width;
        condition = (index) => index < board.grid.length;
        break;
    }

    var startingPosition = player.position;
    var startingTileValue = board.grid[player.position];

    // Do not move if human player collided the border
    if (!condition(player.position + delta) ||
        board.grid[player.position + delta] == null) {
      if (direction != null) return;
      player.moves++;
    } else {
      player.position += delta;
      player.moves++;
      // Update tiles
      if (board.grid[startingPosition] != 0) {
        for (var tile = player.position; condition(tile); tile += delta) {
          if (board.grid[tile] == 0 || board.grid[tile] == null) continue;
          var previousValue = board.grid[tile];
          board.grid[tile] = (board.grid[tile] - startingTileValue).abs();
          if (board.grid[tile] == 0) {
            _updatePoints(tile, player, startingTileValue);
            board.grid[startingPosition] = 0;
          } else if (match.board.grid[tile] <=
              (isLevelMode ? 2 : board.biggestLow)) {
            board.grid[tile] = previousValue + startingTileValue;
          }
        }
      }
    }
    _someoneMoved();
  }

  // Update player points
  void _updatePoints(int tile, Player player, int deletedTileValue) {
    player.points += 2 * deletedTileValue;
    alivePlayers.forEach((p) {
      if (p != player && p.position == tile && p.moves != 0) {
        p.alive = false;
        player.kills++;
        player.points += 10;
      }
    });
  }

  // Update game page state and check match status
  void _someoneMoved() {
    if (gameSpeed == 0 && ++_playerIndexTurn >= alivePlayers.length) {
      _playerIndexTurn = 0;
    }
    if (_gamePage.mounted) _gamePage.update();
    _checkMatchStatus();
  }

  // Check if someone won the match
  void _checkMatchStatus() {
    if (!playerOne.alive) {
      endDialog(_gamePage, win: false, byDeletion: true);
    } else if (isMultiMode && alivePlayers.length == 1) {
      endDialog(_gamePage, win: true, winner: playerOne, byDeletion: true);
    } else if (board.remainingTiles == 0) {
      switch (mode) {
        case GameMode.level:
          if (playerOne.moves <= level.maxMoves) {
            _saveData();
            saveNextLevel(level.levelId);
            endDialog(_gamePage, win: true);
          } else {
            endDialog(_gamePage, win: false);
          }
          break;
        case GameMode.single:
          endDialog(_gamePage, win: true);
          _saveData();
          break;
        case GameMode.multi:
          endDialog(_gamePage,
              win: ranking.first == playerOne,
              winner: ranking.first,
              byDeletion: false);
          break;
      }
    }
  }

  // Save match result
  void _saveData() {
    saveResult(Result(
        startDate: initDate,
        endDate: DateTime.now().millisecondsSinceEpoch,
        matchId: id ?? -(level.levelId[0] * 7 + (level.levelId[1] + 1)),
        sideLength: board.width,
        moves: playerOne.moves,
        points: playerOne.points,
        minBoardInt: board.minValue,
        maxBoardInt: board.maxValue,
        biggestLow: board.biggestLow));
  }
}

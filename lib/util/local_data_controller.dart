import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:zoysii/models/level.dart';
import 'package:zoysii/models/result.dart';
import 'package:zoysii/models/settings.dart';

// Game settings
Settings settings;

// List of match results
var results = <Result>[];

// Next level to play
// nextLevel[0] == layer, nextLevel[1] == level
var nextLevel = List<int>(2);

// Game levels and layers (groups of levels)
var layers = <String>[];
var levels = <Level>[];

// Virtual gamepad position
Offset gamepadOffset;

SharedPreferences _prefs;

// Import all data from shared preferences
Future<void> loadStoredData() async {
  _prefs = await SharedPreferences.getInstance();
  _loadResults();
  _loadSettings();
  _loadNextLevel();
  _loadLevels();
}

// Save a new result in shared preferences
void saveResult(Result result) {
  results.add(result);
  _saveResults();
}

// Save results in shared preferences
void _saveResults() {
  var _jsonResults = <dynamic>[];
  results.forEach((result) {
    _jsonResults.add(result.toJson());
  });
  _prefs.setString('results_v3', jsonEncode(_jsonResults));
}

// Restore results from shared preferences
void _loadResults() {
  if (_prefs.containsKey('results_v3')) {
    List<dynamic> _jsonResults =
        jsonDecode(_prefs.getString('results_v3') ?? '[]');
    _jsonResults.forEach((jResult) {
      results.add(Result.fromJson(jResult));
    });
  } else if (_prefs.containsKey('results_v2')) {
    // Restore results from old app versions (2.0.x)
    List<dynamic> _jsonResults =
        jsonDecode(_prefs.getString('results_v2') ?? '[]');
    _jsonResults.forEach((oldResult) {
      results.add(Result.fromV2(oldResult));
    });
    if (results.isNotEmpty) _saveResults();
  } else if (_prefs.containsKey('results')) {
    // Restore results from old app versions (<2.0.0)
    List<dynamic> _oldResults = jsonDecode(_prefs.getString('results') ?? '[]');
    _oldResults.forEach((oldResult) {
      results.add(Result.fromV1(oldResult));
    });
    if (results.isNotEmpty) _saveResults();
  }
}

// Save app settings in shared preferences
void saveSettings() {
  _prefs.setString('settings', jsonEncode(settings.toJson()));
}

void saveGamepadOffset() {
  _prefs.setDouble('gamepadOffsetX', gamepadOffset.dx);
  _prefs.setDouble('gamepadOffsetY', gamepadOffset.dy);
}

// Restore app settings
void _loadSettings() {
  settings = Settings(jsonDecode(_prefs.getString('settings') ?? '{}'));
  gamepadOffset = Offset(_prefs.getDouble('gamepadOffsetX') ?? 0,
      _prefs.getDouble('gamepadOffsetY') ?? 0);
}

// Save next level to play in shared preferences
void saveNextLevel(List<int> levelWon) {
  if (listEquals(levelWon, nextLevel)) {
    ++nextLevel[1];
    if (nextLevel[1] > 6) {
      ++nextLevel[0];
      nextLevel[1] = 0;
    }
    _prefs.setInt('lastWorldSolved', nextLevel[0]);
    _prefs.setInt('lastLevelSolved', nextLevel[1]);
  }
}

// Restore next level to play
void _loadNextLevel() {
  nextLevel[0] = _prefs.getInt('lastWorldSolved') ?? 0;
  nextLevel[1] = _prefs.getInt('lastLevelSolved') ?? 0;
}

// Load levels from assets
void _loadLevels() async {
  var data = json.decode(await rootBundle.loadString('assets/levels.json'));
  layers.addAll(data['layers'].cast<String>());
  data['levels'].forEach((level) {
    levels.add(Level(level));
  });
}

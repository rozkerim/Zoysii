import 'package:meta/meta.dart';

import 'package:zoysii/models/numeral_system.dart';

// Extension used to handle conversion to supported number notations
extension Notations on int {
  // Convert input number to a string representation in the given notation
  String toNotation(NumeralSystem notation, {bool isPlayer}) {
    if (this == 0) return notation != NumeralSystem.Braille ? '•' : '⠚';
    if (this == null) return ' ';
    var _sign = '';
    var value = this;
    if (this < 0) {
      _sign = '-';
      value = value.abs();
    }
    switch (notation) {
      case NumeralSystem.Arabic:
        return _sign + value.toString();
      case NumeralSystem.Hex:
        return _sign + value.toRadixString(16).toUpperCase();
      case NumeralSystem.Letters:
        return _sign + value._toLetter();
      case NumeralSystem.Aegean:
        return _sign + value._toAegean();
      case NumeralSystem.Armenian:
        return _sign + value._toArmenian();
      case NumeralSystem.Khmer:
        return _sign + value._toKhmer();
      case NumeralSystem.Braille:
        return _sign + value._toBraille();
      case NumeralSystem.Burmese:
        return _sign + value._toBurmese();
      case NumeralSystem.Farsi:
        return _sign + value._toFarsi();
      default:
        return ':(';
    }
  }

  // Convert int to its letter representation
  // A == 1, B == 2, Z == 26, AA == 27, AB == 28, ...
  String _toLetter() {
    if (this > 702) return toString();
    var alphabet = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var intDivide = this ~/ 26;
    var remainder = this % 26;
    return (alphabet[intDivide - (remainder != 0 ? 0 : 1)] +
            alphabet[remainder != 0 ? remainder : 26])
        .trim();
  }

  // Convert this into Khmer number
  String _toKhmer() => _replaceWith('០១២៣៤៥៦៧៨៩');

  // Convert this into Braille number
  String _toBraille() => _replaceWith('⠚⠁⠃⠉⠙⠑⠋⠛⠓⠊');

  // Convert this into Farsi number
  String _toFarsi() => _replaceWith('۰۱۲۳۴۵۶۷۸۹');

  // Convert this into Burmese number
  String _toBurmese() => _replaceWith('၀၁၂၃၄၅၆၇၈၉');

  // Convert this into Roman number
  /*String _toRoman() => _replaceWithUTH(
      U: ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'],
      T: ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'],
      H: ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM']);*/

  // Convert this into Aegean number
  String _toAegean() => _replaceWithUTH(
      U: ['', '𐄇', '𐄈', '𐄉', '𐄊', '𐄋', '𐄌', '𐄍', '𐄎', '𐄏'],
      T: ['', '𐄐', '𐄑', '𐄒', '𐄓', '𐄔', '𐄕', '𐄖', '𐄗', '𐄘'],
      H: ['', '𐄙', '𐄚', '𐄛', '𐄜', '𐄝', '𐄞', '𐄟', '𐄠', '𐄡']);

  // Convert this into Armenian number
  String _toArmenian() => _replaceWithUTH(
      U: ['', 'Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ'],
      T: ['', 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ'],
      H: ['', 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ']);

  // Convert this into Chinese number
  /*String _toChinese() {
    var thisAsString = toString();
    var output = '';
    var I = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
    var X = ['', '十', '百'];
    for (var i = 0; i < thisAsString.length; i++) {
      if (thisAsString[i] == '1' && i == 0 && i < thisAsString.length - 1) {
        output += X[thisAsString.length - 1];
        continue;
      }
      output += I[int.parse(thisAsString[i])] + X[thisAsString.length - 1 - i];
    }
    return output;
  }*/

  // Replace digits with input notation
  String _replaceWith(String notation) =>
      toString().split('').fold('', (prev, e) => prev + notation[int.parse(e)]);

  // Replace digits with input notation for units (U), tens (T) and hundreds (H)
  String _replaceWithUTH(
          {@required List<String> U,
          @required List<String> T,
          @required List<String> H}) =>
      this > 999
          ? toString()
          : H[((this % 1000) ~/ 100)] + T[((this % 100) ~/ 10)] + U[this % 10];
}

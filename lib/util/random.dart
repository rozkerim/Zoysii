import 'dart:math';

Random _currentRandom;

int setRandom(int inputSeed) {
  var _currentSeed =
      (inputSeed ?? -1) == -1 ? Random().nextInt(9999999) : inputSeed;
  _currentRandom = Random(_currentSeed);
  return _currentSeed;
}

int getRandom(int min, int max) => min + _currentRandom.nextInt(max - min + 1);

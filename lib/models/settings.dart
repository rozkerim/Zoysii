import 'dart:io';
import 'package:flutter/material.dart';

import 'package:zoysii/main.dart';
import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Class used to store settings data
class Settings {
  // Grid side length
  int sideLength;

  // Selected input method
  int inputMethod;

  // Virtual gamepad dimension
  double gamepadSize;

  // Virtual gamepad shape
  int gamepadShape;

  // Board range values
  int maxBoardInt;
  int minBoardInt;

  // Biggest "low number" for rule 3b
  int biggestLow;

  // Numeral notation
  int _numeralSystem;
  NumeralSystem get numeralSystem => NumeralSystem.values[_numeralSystem];
  set numeralSystem(NumeralSystem value) {
    _numeralSystem = value.index;
  }

  // App theme
  ThemeMode theme;

  // Multiplayer preferences
  int playersMulti;
  int speedMulti;

  // True if it is the first run of the app
  bool firstRun;

  // App language
  String _languageCode;
  bool get useSystemLanguage => _languageCode == null;
  String get _currentLanguageCode => _languageCode ?? Platform.localeName;
  Locale get locale =>
      supportedLocales.contains(Locale(_currentLanguageCode.split('_').first))
          ? Locale(_currentLanguageCode.split('_').first)
          : const Locale('en');
  set locale(Locale locale) {
    _languageCode = locale?.toString();
  }

  Settings(Map<String, dynamic> json)
      : sideLength = json['sideLength'] ?? 6,
        inputMethod = json['inputMethod'] ?? 0,
        gamepadSize = json['gamepadSize'] ?? 50,
        gamepadShape = json['gamepadShape'] ?? 0,
        minBoardInt = json['minBoardInt'] ?? 1,
        maxBoardInt = json['maxBoardInt'] ?? 19,
        biggestLow = json['biggestLow'] ?? 2,
        _numeralSystem = json['numeralSystem'] ?? 0,
        playersMulti = json['playersMulti'] ?? 3,
        speedMulti = json['speedMulti'] ?? 0,
        firstRun = json['firstRun'] ?? (results?.isEmpty ?? true),
        theme = ThemeMode
            .values[json['theme'] ?? ((json['darkTheme'] ?? false) ? 2 : 0)],
        _languageCode = json['languageCode'];

  Map<String, dynamic> toJson() => {
        'sideLength': sideLength,
        'inputMethod': inputMethod,
        'gamepadSize': gamepadSize,
        'gamepadShape': gamepadShape,
        'minBoardInt': minBoardInt,
        'maxBoardInt': maxBoardInt,
        'biggestLow': biggestLow,
        'numeralSystem': _numeralSystem,
        'theme': theme.index,
        'playersMulti': playersMulti,
        'speedMulti': speedMulti,
        'firstRun': firstRun,
        'languageCode': _languageCode,
      };
}

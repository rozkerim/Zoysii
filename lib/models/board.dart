import 'dart:math';

import 'package:zoysii/models/level.dart';
import 'package:zoysii/util/local_data_controller.dart';
import 'package:zoysii/util/random.dart';

class Board {
  // List of tiles
  List<int> grid;

  // Board settings
  int width;
  int height;
  int sideLength;
  int minValue;
  int maxValue;
  int biggestLow;

  // True if every tile should be deleted (eg level mode)
  bool _deleteAll = false;

  Board({
    this.biggestLow,
    this.sideLength,
    this.minValue,
    this.maxValue,
    this.width,
    this.height,
  }) {
    minValue ??= settings.minBoardInt;
    maxValue ??= settings.maxBoardInt;
    biggestLow ??= settings.biggestLow;
  }

  // Number of zeros in the board
  int get _zeros => grid.where((tile) => tile == 0 || tile == null).length;

  // Tiles still to be deleted
  int get remainingTiles => _deleteAll
      ? grid.length - _zeros
      : max(width * (height - 1) - _zeros - 1, 0);

  // Create grid tiles
  void createGrid([Level level]) {
    if (level == null) {
      width ??= settings.sideLength;
      height ??= width;
      grid = List.generate(
          width * height, (index) => getRandom(minValue, maxValue));
    } else {
      _deleteAll = true;
      grid = List.from(level.grid);
      width = level.width ?? sqrt(grid.length).toInt();
      height = grid.length ~/ width;
    }
  }
}

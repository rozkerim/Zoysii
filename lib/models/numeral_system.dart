import 'package:flutter/foundation.dart';

import 'package:zoysii/i18n/strings.i18n.dart';

// List of possible number representations
// i18n: 'Arabic'.i18n, 'Hex'.i18n, 'Letters'.i18n, 'Aegean'.i18n,
// 'Braille'.i18n, 'Khmer'.i18n, 'Armenian'.i18n, 'Burmese'.i18n, 'Farsi'.i18n,
enum NumeralSystem {
  Arabic,
  Hex,
  Letters,
  Aegean,
  Braille,
  Khmer,
  Armenian,
  Burmese,
  Farsi
}

extension NumeralSystemExtension on NumeralSystem {
  String get name => describeEnum(this).i18n;
  String get fontFamily {
    switch (this) {
      case NumeralSystem.Aegean:
        return 'NotoSansLinearB';
      case NumeralSystem.Armenian:
        return 'NotoSansArmenian';
      case NumeralSystem.Khmer:
        return 'NotoSansKhmer';
      case NumeralSystem.Burmese:
        return 'NotoSansMyanmar';
      default:
        return null;
    }
  }
}

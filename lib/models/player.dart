import 'package:flutter/material.dart';

class Player {
  // Player position
  int position;

  // Player color
  final Color _color;
  Color get color => alive ? _color : Colors.grey[500];

  // Player name
  String name;

  // True if this is controlled by a human
  final bool human;

  Player({
    @required this.position,
    @required int color,
    @required this.name,
    this.human = true,
  }) : _color = Color(color);

  // Moves count
  int moves = 0;

  // Earned points
  int points = 0;

  // Player status
  bool alive = true;

  // Number of deleted players
  int kills = 0;
}

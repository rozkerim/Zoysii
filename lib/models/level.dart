import 'package:zoysii/util/direction.dart';

// Game level
class Level {
  // Level name
  String name;

  // levelId[0] == layer, levelId[1] == level
  List<int> levelId;

  // Moves allowed in this level
  int maxMoves;

  // Theoretical minimum number of moves
  int minMoves;

  // First moves of the solution
  List<Direction> hint;

  // Game grid
  List<int> grid;

  // Board width
  int width;

  // Player starting position
  int startingPosition;

  Level(Map<String, dynamic> json)
      : name = json['name'],
        levelId = json['level'].cast<int>(),
        maxMoves = json['max_moves'],
        minMoves = json['min_moves'] ?? json['max_moves'],
        hint = List.from((json['hint'] ?? []).map((v) => Direction.values[v])),
        startingPosition = json['starting_position'],
        width = json['width'],
        grid = json['grid'].cast<int>();
}

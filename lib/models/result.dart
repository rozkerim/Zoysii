import 'dart:math';

import 'package:flutter/material.dart';

// Match result
class Result {
  // Match id
  // In levels mode it equals to: -(world * 7 + level + 1)
  int matchId;

  // Match start date, saved as milliseconds since epoch
  int startDate;

  // Match end date, saved as milliseconds since epoch
  int endDate;

  // Match duration
  Duration get _duration => Duration(milliseconds: endDate - startDate);
  String get formattedDuration =>
      _duration.inMinutes.remainder(60).toString().padLeft(2, '0') +
      ':' +
      _duration.inSeconds.remainder(60).toString().padLeft(2, '0');

  // Moves done by player one
  int moves;

  // Points scored by player one
  int points;

  // Match grid size length
  int sideLength;

  // Board range of values
  int minBoardInt = 1;
  int maxBoardInt = 19;

  // Biggest "low number" for rule 3b
  int biggestLow = 2;

  Result({
    @required this.matchId,
    @required this.startDate,
    @required this.endDate,
    @required this.sideLength,
    @required this.moves,
    @required this.points,
    @required this.minBoardInt,
    @required this.maxBoardInt,
    @required this.biggestLow,
  });

  // Import result from a Map<String, dynamic>
  Result.fromJson(Map<String, dynamic> json)
      : matchId = json['matchId'] ?? 0,
        startDate = json['startDate'] ?? 0,
        endDate = json['endDate'] ?? 0,
        sideLength = json['sideLength'] ?? 0,
        moves = json['moves'] ?? 0,
        points = json['points'] ?? 0,
        minBoardInt = json['minBoardInt'] ?? 0,
        maxBoardInt = json['maxBoardInt'] ?? 0,
        biggestLow = json['biggestLow'] ?? 0;

  // Import result from old app versions (2.0.x)
  Result.fromV2(Map<String, dynamic> json)
      : this.fromJson(() {
          json['points'] = max((((json['points'] ?? 0) - 1) ~/ 2) as int, 0);
          return json;
        }());

  // Import result from old app versions (1.x)
  Result.fromV1(List<dynamic> oldResult)
      : startDate = oldResult[0],
        endDate = oldResult[1],
        matchId = oldResult[2],
        sideLength = oldResult[3],
        moves = oldResult[4],
        points = (oldResult[5] - 1) ~/ 2 {
    // Get board settings if app version was >=1.2.0 and <2.0.0
    if (oldResult.length > 6) {
      minBoardInt = oldResult[6];
      maxBoardInt = oldResult[7];
      biggestLow = oldResult[8];
    }
  }

  // Export result as a Map<String, dynamic>
  Map<String, dynamic> toJson() => {
        'matchId': matchId,
        'startDate': startDate,
        'endDate': endDate,
        'sideLength': sideLength,
        'moves': moves,
        'points': points,
        'minBoardInt': minBoardInt,
        'maxBoardInt': maxBoardInt,
        'biggestLow': biggestLow,
      };
}

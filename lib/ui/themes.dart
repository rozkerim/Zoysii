import 'package:flutter/material.dart';

final lightTheme = ThemeData(
  appBarTheme: const AppBarTheme(
    textTheme: TextTheme(
      headline6: TextStyle(
          letterSpacing: 6, fontSize: 22, fontWeight: FontWeight.w600),
    ),
  ),
  primarySwatch: Colors.blueGrey,
  primaryColor: const Color(0xFF244A63),
  accentColor: Colors.black,
  brightness: Brightness.light,
  textSelectionColor: Colors.grey[700].withOpacity(0.4),
  snackBarTheme: const SnackBarThemeData(
    elevation: 0,
    contentTextStyle: TextStyle(fontSize: 18),
  ),
  dividerColor: Colors.black38,
);

final darkTheme = ThemeData(
  appBarTheme: const AppBarTheme(
    textTheme: TextTheme(
      headline6: TextStyle(
          letterSpacing: 6, fontSize: 22, fontWeight: FontWeight.w600),
    ),
  ),
  primarySwatch: Colors.blueGrey,
  primaryColor: const Color(0xFF244A63),
  accentColor: Colors.white,
  brightness: Brightness.dark,
  textSelectionColor: Colors.grey[700].withOpacity(0.4),
  snackBarTheme: const SnackBarThemeData(
    elevation: 0,
    contentTextStyle: TextStyle(color: Colors.black, fontSize: 18),
  ),
  dividerColor: Colors.white70,
);

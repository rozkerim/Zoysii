import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/basic.dart';

// Floating "PLAY" button
class PlayButton extends StatelessWidget {
  final int playerCount;
  final int gameSpeed;
  final bool gradient;
  final VoidCallback callback;
  PlayButton(
      {this.playerCount = 1,
      this.gameSpeed,
      this.gradient = false,
      this.callback});

  final _fabBorderRadius = const BorderRadius.all(Radius.circular(30));

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 50,
      decoration: BoxDecoration(
          gradient: gradient ? appGradient : null,
          color: gradient ? null : const Color(0xFF244A63),
          borderRadius: _fabBorderRadius),
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: _fabBorderRadius),
        child: Text('Play'.i18n.toUpperCase(),
            style: const TextStyle(
                color: Colors.white, fontSize: 20, letterSpacing: 8)),
        onPressed: () {
          Navigator.push(
              context,
              FadeRoute(
                GamePage(
                  (playerCount == 1) ? GameMode.single : GameMode.multi,
                  playerCount: playerCount,
                  gameSpeed: gameSpeed,
                ),
              )).then((_) => callback?.call());
        },
      ),
    );
  }
}

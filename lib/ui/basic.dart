import 'package:flutter/material.dart';

// Colors used for each player
const playerColor = <int>[0xFFD44A63, 0xFF246AC3, 0xFFF49A03, 0xFF248A63];

// Linear gradient used in app
const appGradient = LinearGradient(colors: [
  Color(0xFF103b4f),
  Color(0xFF1c3b4f),
  Color(0xFF293b4f),
  Color(0xFF433b4f),
  Color(0xFF5c3b4f),
  Color(0xFF693b4f),
  Color(0xFF763b4f),
], begin: Alignment.topLeft, end: Alignment.bottomRight);

const darkAppGradient = LinearGradient(colors: [
  Colors.black12,
  Colors.black26,
  Colors.black38,
  Colors.black45,
], begin: Alignment.topLeft, end: Alignment.bottomRight);

// Shape used for menu buttons
const menuButtonShape =
    RoundedRectangleBorder(borderRadius: BorderRadius.all(radius));

// Radius used for dialog and cards
const radius = Radius.circular(20);
const radiusFive = Radius.circular(5);
const borderRadius = BorderRadius.only(
    topRight: radius,
    topLeft: radiusFive,
    bottomLeft: radius,
    bottomRight: radiusFive);

// Page route
class FadeRoute extends PageRouteBuilder {
  @override
  final Duration transitionDuration = const Duration(milliseconds: 150);
  final Widget page;

  FadeRoute(this.page)
      : super(
            pageBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
            ) =>
                page,
            transitionsBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child,
            ) =>
                FadeTransition(opacity: animation, child: child));
}

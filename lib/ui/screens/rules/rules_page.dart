import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/rules/util/rule.dart';
import 'package:zoysii/ui/screens/rules/widgets/rule_page.dart';
import 'package:zoysii/util/local_data_controller.dart';

class RulesPage extends StatefulWidget {
  final StatelessWidget homePage;
  RulesPage([this.homePage]);

  @override
  _RulesPageState createState() => _RulesPageState();
}

class _RulesPageState extends State<RulesPage>
    with SingleTickerProviderStateMixin {
  List<Rule> rules() => [
        Rule(
          title: 'Z O Y S I I',
          subtitle:
              'Zoysii is a simple logic game.\nSwipe to right to learn how to play.'
                  .i18n,
          image: 'assets/graphics/rule_0.png',
          lightImage: 'assets/graphics/rule_0_white.png',
          border: false,
        ),
        Rule(
          title: 'You'.i18n,
          subtitle:
              'You are the red tile on a square board.\nIn the game you can move yourself by swiping horizontally or vertically.'
                  .i18n,
          image: 'assets/graphics/rule_1.png',
        ),
        Rule(
          title: 'Moves'.i18n,
          subtitle:
              'When you move, your starting tile value is subtracted from each of the tiles in the direction you are going.'
                  .i18n,
          image: 'assets/graphics/rule_2.gif',
        ),
        Rule(
          title: 'Low numbers'.i18n,
          subtitle:
              'Negative numbers become positive.\n\nMoreover, if the value of a tile would be equal to 1 or 2 there will be an increase instead of a decrease. (4 - 5 -> 9)'
                  .i18n,
          image: 'assets/graphics/rule_3.gif',
        ),
        Rule(
          title: 'Points'.i18n,
          subtitle:
              'If the value of a tile becomes equal to zero, starting tile value becomes zero too.\n\nYou earn as many points as the value of the deleted tiles, with bonus points if you delete more than two tiles at once.'
                  .i18n,
          image: 'assets/graphics/rule_4.gif',
        ),
        Rule(
          title: 'How to win'.i18n,
          subtitle:
              "The aim is to delete almost every tile while trying to make the most points.\n\nIn multiplayer matches a player can also win by deleting opponent's tile."
                  .i18n,
          image: 'assets/graphics/rule_5.gif',
        ),
        Rule(
          title: "That's all".i18n,
          subtitle:
              'The best way to learn is by playing!\n\nLevels mode is a good place to start.'
                  .i18n,
          border: false,
        ),
      ];

  TabController _controller;

  @override
  void initState() {
    _controller = TabController(vsync: this, length: rules().length);
    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading:
            _controller.index >= _controller.length - 1 || settings.firstRun
                ? Container()
                : IconButton(
                    icon: Icon(Icons.close,
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.white70
                            : Colors.black87,
                        size: 32),
                    onPressed: () {
                      if (widget.homePage == null) {
                        Navigator.pop(context);
                      } else {
                        onExit();
                        Navigator.pushReplacement(
                            context, FadeRoute(widget.homePage));
                      }
                    },
                  ),
      ),
      body: TabBarView(
          controller: _controller,
          children: [for (var rule in rules()) RulePage(rule)]),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                child: Row(
                  children: <Widget>[
                    const Icon(Icons.navigate_before),
                    Text('Back'.i18n),
                  ],
                ),
                onPressed: (_controller.index > 0)
                    ? () {
                        _controller.index -= (_controller.index > 0) ? 1 : 0;
                      }
                    : null,
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      for (var i = 1; i < _controller.length - 1; i++)
                        CircleAvatar(
                          radius: _controller.index == i ? 4 : 3,
                          backgroundColor: _controller.index == i
                              ? (Theme.of(context).brightness == Brightness.dark
                                  ? Colors.white
                                  : Colors.black)
                              : Colors.grey,
                        ),
                    ],
                  ),
                ),
              ),
              _controller.index >= _controller.length - 1
                  ? FlatButton(
                      color: Colors.deepPurple[400],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      child: Row(
                        children: <Widget>[
                          Text(
                            'Ok'.i18n.toUpperCase(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                      onPressed: () {
                        if (widget.homePage == null) {
                          Navigator.pop(context);
                        } else {
                          onExit();
                          Navigator.pushReplacement(
                              context, FadeRoute(widget.homePage));
                        }
                      },
                    )
                  : FlatButton(
                      child: Row(
                        children: <Widget>[
                          Text('Next'.i18n),
                          const Icon(Icons.navigate_next)
                        ],
                      ),
                      onPressed: () {
                        if (_controller.index < _controller.length - 1) {
                          ++_controller.index;
                        }
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }

  void onExit() {
    if (settings.firstRun) {
      settings.firstRun = false;
      saveSettings();
    }
  }
}

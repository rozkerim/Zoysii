import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/util/local_data_controller.dart';
import 'package:zoysii/ui/widgets/toast.dart';

// Dialog that shows a radio list to select numeral system
Future numeralSystemDialog(SettingsPageState settingsPageState) {
  return showDialog(
    context: settingsPageState.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Select a Numeral System'.i18n),
        contentPadding: const EdgeInsets.symmetric(vertical: 20),
        content: SingleChildScrollView(
          child: Column(
            children: List.generate(NumeralSystem.values.length, (index) {
              return RadioListTile<int>(
                title: Text(
                  NumeralSystem.values[index].name +
                      (index > nextLevel[0] ? ' ' + '(locked)'.i18n : ''),
                ),
                value: index,
                groupValue:
                    NumeralSystem.values.indexOf(settings.numeralSystem),
                onChanged: (int value) {
                  if (index > nextLevel[0]) {
                    Toast.show(
                        'You have to complete Layer %s in levels mode to unlock this feature'
                            .i18n
                            .fill([index]),
                        context);
                  } else {
                    settings.numeralSystem = NumeralSystem.values[value];
                    settingsPageState.refresh();
                    saveSettings();
                    Navigator.of(context).pop();
                  }
                },
              );
            }),
          ),
        ),
      );
    },
  );
}

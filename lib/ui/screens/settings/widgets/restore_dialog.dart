import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/settings.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Dialog used to restore default settings
Future restoreSettingsDialog(SettingsPageState settingsPageState) {
  return showDialog(
    context: settingsPageState.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Restore default settings?'.i18n),
        content: Text(
            'Are you sure you wish to delete your settings and restore default ones?'
                .i18n),
        actions: <Widget>[
          FlatButton(
            child: Text('Restore'.i18n),
            textColor: Theme.of(context).accentColor,
            onPressed: () {
              settings = Settings({'firstRun': false});
              I18n.of(context).locale = settings.locale;
              saveSettings();
              settingsPageState.fullRefresh();
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text('Cancel'.i18n),
            textColor: Theme.of(context).accentColor,
            onPressed: Navigator.of(context).pop,
          ),
        ],
      );
    },
  );
}

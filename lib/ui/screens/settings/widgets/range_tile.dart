import 'package:flutter/material.dart';

import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Expansion tile widget for options that use range slider
class RangeTile extends StatelessWidget {
  // Tile title
  final String title;

  // Tile subtitle
  final String subtitle;

  // State of the settings page
  final SettingsPageState settingsPageState;

  RangeTile(this.title, this.subtitle, {@required this.settingsPageState});

  @override
  Widget build(BuildContext context) {
    var rangeValues = RangeValues(
        settings.minBoardInt.toDouble(), settings.maxBoardInt.toDouble());

    return ExpansionTile(
      title: Text(title, style: const TextStyle(fontSize: 20)),
      subtitle: Text(
        subtitle,
        style: TextStyle(color: Theme.of(context).textTheme.caption.color),
      ),
      trailing: Text(
        '${settings.minBoardInt} - ${settings.maxBoardInt}',
        style: const TextStyle(fontSize: 16),
      ),
      initiallyExpanded: false,
      children: <Widget>[
        RangeSlider(
          values: rangeValues,
          min: 0,
          max: 50,
          onChanged: (RangeValues values) {
            if (values.end - values.start >= 15) {
              rangeValues = RangeValues(values.start.round().toDouble(),
                  values.end.round().toDouble());
            } else {
              if (rangeValues.start == values.start) {
                rangeValues =
                    RangeValues(rangeValues.start, rangeValues.start + 15);
              } else {
                rangeValues =
                    RangeValues(rangeValues.end - 15, rangeValues.end);
              }
            }
            settings.minBoardInt = rangeValues.start.round();
            settings.maxBoardInt = rangeValues.end.round();
            saveSettings();
            settingsPageState.refresh();
          },
        ),
      ],
    );
  }
}

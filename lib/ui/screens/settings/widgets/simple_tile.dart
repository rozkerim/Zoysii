import 'package:flutter/material.dart';

import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/ui/screens/settings/widgets/numeral_system_dialog.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Returns a list tile. Used only for numeral system option, for now
class SimpleTile extends StatelessWidget {
  // Tile title
  final String title;

  // Tile subtitle
  final String subtitle;

  // State of the settings page
  final SettingsPageState settingsPageState;

  SimpleTile(this.title, this.subtitle, {@required this.settingsPageState});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title, style: const TextStyle(fontSize: 20)),
      trailing: Text(
        settings.numeralSystem.name,
        style: const TextStyle(fontSize: 16),
      ),
      subtitle: Text(subtitle),
      onTap: () => numeralSystemDialog(settingsPageState),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'package:zoysii/main.dart';
import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/ui/screens/settings/resources/languages.dart';
import 'package:zoysii/ui/screens/settings/settings_page.dart';
import 'package:zoysii/ui/screens/settings/widgets/drop_down_tile.dart';
import 'package:zoysii/ui/screens/settings/widgets/range_tile.dart';
import 'package:zoysii/ui/screens/settings/widgets/simple_tile.dart';
import 'package:zoysii/util/local_data_controller.dart';

// List of available options displayed in settings page
class OptionList extends StatelessWidget {
  // State of the settings page
  final SettingsPageState settingsPageState;

  OptionList(this.settingsPageState);

  // List of possible grid sizes
  final List<int> _gridSizes = [5, 6, 7, 8, 9, 10];

  // List of possible biggest "low numbers"
  final List<int> _lowNumberList = [0, 1, 2, 3, 4, 5];

  // List of possible input methods
  final Map<int, String> _inputMethods = {
    0: 'Gestures'.i18n,
    1: 'Gamepad'.i18n
  };

  // List of possible virtual gamepad sizes
  final Map<int, String> gamepadSizes = {
    30: 'Smallest'.i18n,
    40: 'Small'.i18n,
    50: 'Default'.i18n,
    60: 'Large'.i18n,
    70: 'Largest'.i18n,
  };

  // List of possible gamepad shapes
  final Map<int, String> gamepadShape = {
    0: 'Classic (＋)'.i18n,
    1: 'WASD (⊥)'.i18n
  };

  // List of possible themes
  final Map<int, String> themes = {
    0: 'System'.i18n,
    1: 'Light'.i18n,
    2: 'Dark'.i18n
  };

  @override
  Widget build(BuildContext context) => ListView(children: <Widget>[
        DropDownTile(
          'sideLength',
          'Grid dimension'.i18n,
          'Size of the game board'.i18n,
          settings.sideLength,
          _gridSizes,
          settingsPageState: settingsPageState,
        ),
        SimpleTile(
          'Default numeral system'.i18n,
          'Numeral notation used by default'.i18n,
          settingsPageState: settingsPageState,
        ),
        Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: ExpansionTile(
            title: Text(
              'Advanced game options'.i18n,
              style: const TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'You should not change these unless you know what you are doing'
                  .i18n,
              style:
                  TextStyle(color: Theme.of(context).textTheme.caption.color),
            ),
            initiallyExpanded: false,
            backgroundColor: Theme.of(context).brightness == Brightness.dark
                ? Colors.grey[900]
                : Colors.grey[100],
            children: <Widget>[
              DropDownTile(
                'biggestLow',
                'Highest "Low number"'.i18n,
                'Choose which low numers are avoided'.i18n,
                settings.biggestLow,
                _lowNumberList,
                settingsPageState: settingsPageState,
              ),
              RangeTile(
                'Range of values'.i18n,
                'Define the range of possible values at the beginning of the game'
                    .i18n,
                settingsPageState: settingsPageState,
              ),
            ],
          ),
        ),
        const Divider(),
        DropDownTile(
          'theme',
          'App Theme'.i18n,
          'Change app theme'.i18n,
          settings.theme.index,
          <int>[0, 1, 2],
          mapText: themes,
          settingsPageState: settingsPageState,
        ),
        DropDownTile(
          'inputMethod',
          'Input method'.i18n,
          'Method used to move'.i18n,
          settings.inputMethod,
          <int>[0, 1],
          mapText: _inputMethods,
          settingsPageState: settingsPageState,
        ),
        if (settings.inputMethod == 1)
          DropDownTile(
            'gamepadSize',
            'Gamepad size'.i18n,
            'Virtual gamepad dimension'.i18n,
            settings.gamepadSize.toInt(),
            gamepadSizes.keys.toList(),
            mapText: gamepadSizes,
            settingsPageState: settingsPageState,
          ),
        if (settings.inputMethod == 1)
          DropDownTile(
            'gamepadShape',
            'Gamepad shape'.i18n,
            'Select gamepad shape'.i18n,
            settings.gamepadShape,
            gamepadShape.keys.toList(),
            mapText: gamepadShape,
            settingsPageState: settingsPageState,
          ),
        ListTile(
          title: Text('Language'.i18n, style: const TextStyle(fontSize: 20)),
          subtitle: Text('App language'.i18n),
          trailing: DropdownButton<Locale>(
            value: settings.useSystemLanguage ? null : settings.locale,
            onChanged: (Locale newValue) {
              settings.locale = newValue;
              I18n.of(context).locale = settings.locale;
              saveSettings();
              settingsPageState.refresh();
            },
            items: () {
              var items = <DropdownMenuItem<Locale>>[
                DropdownMenuItem<Locale>(
                  value: null,
                  child: Text('System default'.i18n),
                )
              ];
              items.addAll(supportedLocales
                  .map<DropdownMenuItem<Locale>>(
                    (value) => DropdownMenuItem<Locale>(
                      value: value,
                      child: Text(languageName.containsKey(value.languageCode)
                          ? languageName[value.languageCode][1]
                          : 'missing name'),
                    ),
                  )
                  .toList());
              return items;
            }(),
          ),
        ),
      ]);
}

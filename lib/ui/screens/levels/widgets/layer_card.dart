import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/models/level.dart';
import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/levels/levels_page.dart';
import 'package:zoysii/util/local_data_controller.dart';
import 'package:zoysii/util/notations.dart';

// Card that contains layer info and levels
class LayerCard extends StatelessWidget {
  final int layer;
  final VoidCallback callback;
  LayerCard(this.layer, this.callback);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        margin: const EdgeInsets.symmetric(vertical: 15),
        child: Center(
          child: Text(layers[layer], style: const TextStyle(fontSize: 40)),
        ),
      ),
      Text(
          '${nextLevel[0] > layer ? 7 : nextLevel[1]} / 7 ' + 'completed'.i18n),
      const Divider(color: Colors.transparent),
      for (var level in (levels.where((level) => level.levelId[0] == layer)))
        LevelTile(layer, level, callback),
      const Divider(color: Colors.transparent),
    ]);
  }
}

// Tile that contains level info
class LevelTile extends StatelessWidget {
  final int layer;
  final Level level;
  final VoidCallback callback;
  LevelTile(this.layer, this.level, this.callback);

  @override
  Widget build(BuildContext context) {
    var unlocked = (layer < nextLevel[0] || level.levelId[1] <= nextLevel[1]);
    return Container(
      color: listEquals(level.levelId, nextLevel)
          ? const Color(0xFF945f7f)
          : unlocked
              ? const Color(0xFF4086b3)
              : Theme.of(context).cardColor,
      child: ListTile(
        leading: CircleAvatar(
          child: Text(
            (level.levelId[1] + 1).toNotation(NumeralSystem.values[layer + 1]),
            style: TextStyle(
              color: unlocked ? Colors.black : Colors.white,
              fontSize: 15,
              fontFamily: NumeralSystem.values[layer + 1].fontFamily,
              fontWeight: FontWeight.w800,
            ),
          ),
          radius: 18,
          backgroundColor: unlocked ? Colors.white : Colors.black87,
        ),
        title: Text(unlocked ? level.name : 'Locked'.i18n,
            style:
                TextStyle(color: unlocked ? Colors.white : null, fontSize: 20)),
        trailing: listEquals(level.levelId, nextLevel)
            ? const Icon(Icons.play_arrow, color: Colors.white)
            : unlocked
                ? const Icon(Icons.done, color: Colors.white)
                : null,
        onTap: unlocked && pageIndex - (nextLevel[0] >= 1 ? 0 : 1) == layer
            ? () {
                Navigator.push(
                  context,
                  FadeRoute(
                    GamePage(GameMode.level, selectedLevel: level.levelId),
                  ),
                ).then((_) => callback());
              }
            : null,
      ),
    );
  }
}

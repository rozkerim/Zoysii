import 'package:flutter/material.dart';

import 'package:zoysii/models/board.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/ui/screens/game/widgets/dialogs.dart';
import 'package:zoysii/ui/screens/game/widgets/game_app_bar.dart';
import 'package:zoysii/ui/screens/game/widgets/game_grid.dart';
import 'package:zoysii/ui/screens/game/widgets/gamepad.dart';
import 'package:zoysii/util/direction.dart';
import 'package:zoysii/util/game.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Current match
Game match;

class GamePage extends StatefulWidget {
  static GamePageState of(BuildContext context) =>
      context.findAncestorStateOfType();

  // Game mode
  final GameMode _mode;

  // Total number of players
  final int playerCount;

  // Match to load
  // Null if it should load a random match
  final int matchToLoad;

  // Level to load
  // Null if !match.isLevelMode
  final List<int> selectedLevel;

  // Game settings
  // Null if it should use default values
  final int gameSpeed;

  // Game board settings
  final Board board;

  GamePage(
    this._mode, {
    this.playerCount = 1,
    this.matchToLoad,
    this.selectedLevel,
    this.gameSpeed,
    this.board,
  });

  @override
  GamePageState createState() => GamePageState();
}

class GamePageState extends State<GamePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  // Variables to manage gestures input
  double _initialX, _initialY, _distanceX, _distanceY = 0;

  @override
  void initState() {
    match = Game(
      this,
      widget._mode,
      widget.playerCount,
      inputId: widget.matchToLoad,
      level: widget.selectedLevel,
      gameSpeed: widget.gameSpeed,
      board: widget.board,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _grid =
        settings.inputMethod == 0 ? gestureDetector(GameGrid()) : GameGrid();

    return WillPopScope(
      child: Scaffold(
        key: scaffoldKey,
        appBar: GameAppBar(this),
        body: Stack(children: <Widget>[
          Container(child: _grid),
          if (settings.inputMethod == 1) FloatingGamepad(inputController),
        ]),
      ),
      onWillPop: () => exitDialog(this),
    );
  }

  // Widget that detects gestures
  StatelessWidget gestureDetector(Widget child) {
    return GestureDetector(
        onHorizontalDragStart: (DragStartDetails details) {
          _initialX = details.globalPosition.dx;
        },
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          if (_initialX != null) {
            _distanceX = details.globalPosition.dx - _initialX;
          }
        },
        onHorizontalDragEnd: (DragEndDetails details) {
          _initialX = 0;
          if (_distanceX != null) {
            inputController(_distanceX > 0 ? Direction.right : Direction.left);
          }
        },
        onVerticalDragStart: (DragStartDetails details) {
          _initialY = details.globalPosition.dy;
        },
        onVerticalDragUpdate: (DragUpdateDetails details) {
          if (_initialY != null) {
            _distanceY = details.globalPosition.dy - _initialY;
          }
        },
        onVerticalDragEnd: (DragEndDetails details) {
          _initialY = 0;
          if (_distanceY != null) {
            inputController(_distanceY > 0 ? Direction.down : Direction.up);
          }
        },
        child: child);
  }

  // Manage horizontal and vertical drag input
  void inputController(Direction direction) {
    if (match.canMove(match.playerOne)) {
      match.movePlayer(match.playerOne, direction);
    }
  }

  // Update game page state
  void update() {
    setState(() {});
  }

  @override
  void dispose() {
    match.timer?.cancel();
    match = null;
    super.dispose();
  }
}

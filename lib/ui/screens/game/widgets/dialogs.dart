import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/models/player.dart';
import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/dialog_basics.dart';
import 'package:zoysii/ui/screens/levels/levels_page.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Pause match dialog
void pauseDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return GameDialog(
          title: 'Pause'.i18n,
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Remaining tiles: %s'
                    .i18n
                    .fill([match.board.remainingTiles])),
                for (var player in match.ranking)
                  PlayerInfo(
                    name: match.isMultiMode ? player.name : null,
                    points: !match.isLevelMode ? player.points : null,
                    moves: player.moves,
                    color: player.color,
                    kills: player.kills,
                  ),
              ],
            ),
          ),
          actions: <Widget>[cancelButton(context)],
        );
      },
    );

// New / repeat match dialog
void newMatchDialog(GamePageState gamePage) => showDialog(
    context: gamePage.context,
    builder: (BuildContext context) {
      String title;
      Widget content;
      List<Widget> actions;

      if (match.isLevelMode || match.loopRepeat) {
        title = 'Restart match'.i18n;
        content = Text('Do you want to restart this match?'.i18n);
        actions = [
          if (match.restartCount > 3 && match.level.hint.isNotEmpty)
            DialogButton(
              text: 'Hint'.i18n.toUpperCase(),
              onPressed: () {
                match.init();
                gamePage.update();
                Navigator.of(gamePage.context).pop();
                gamePage.scaffoldKey.currentState.removeCurrentSnackBar();
                gamePage.scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    duration: const Duration(seconds: 6),
                    content: Text(
                      match.level.hint.fold(
                              'Try this:'.i18n + ' ',
                              (previous, element) =>
                                  previous +
                                  describeEnum(element).i18n +
                                  ', ') +
                          '...',
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              },
            ),
          cancelButton(context),
          restartButton(gamePage, color: Color(playerColor[1])),
        ];
      } else {
        var _textFieldController =
            TextEditingController(text: match.id.toString());
        title = 'New Match'.i18n;
        content = TextFormField(
            textAlign: TextAlign.center,
            controller: _textFieldController,
            decoration: InputDecoration(
              helperText:
                  'Insert the Match ID to load.\nLeave it as it is to load a random match.'
                      .i18n,
            ),
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
              LengthLimitingTextInputFormatter(7)
            ]);
        actions = [
          cancelButton(context),
          DialogButton(
            text: 'New Match'.i18n.toUpperCase(),
            onPressed: () {
              Navigator.of(context).pop();
              var matchIdInput = int.parse(_textFieldController.text.isEmpty
                  ? '-1'
                  : _textFieldController.text);
              match.init(
                  newMatchId: matchIdInput == match.id ? -1 : matchIdInput);
              gamePage.update();
            },
          ),
          restartButton(
            gamePage,
            color: Color(playerColor[1]),
          ),
        ];
      }
      return GameDialog(title: title, content: content, actions: actions);
    });

// Exit match dialog
// It does not display the dialog if playerOne.moves <= 1
Future<bool> exitDialog(GamePageState gamePage) {
  if (match.playerOne.moves <= 1) {
    Navigator.pop(gamePage.context);
    return null;
  } else {
    return showDialog(
      context: gamePage.context,
      builder: (BuildContext context) {
        return GameDialog(
          title: 'Exit'.i18n,
          content: Text(
              'Are you sure you want to quit this game?\nMatch data will be lost.'
                  .i18n),
          actions: <Widget>[
            cancelButton(gamePage.context),
            exitButton(context, gamePage, color: Color(playerColor[1])),
          ],
        );
      },
    );
  }
}

// End match dialog
void endDialog(GamePageState gamePage,
    {@required bool win, Player winner, bool byDeletion}) {
  if (match.isPause) return;
  match.isPause = true;
  String title;
  Color titleColor;
  String content;

  switch (match.mode) {
    case GameMode.level:
      if (win) {
        title = 'Level solved'.i18n;
        content = 'You solved the level by doing %s moves.'
            .i18n
            .fill([match.playerOne.moves]);
        if (match.playerOne.moves > match.level.minMoves) {
          content += '\n' +
              "You could have solved it in fewer moves, but it's ok.".i18n;
        } else if (match.playerOne.moves < match.level.maxMoves) {
          content += '\n' +
              'It took you fewer moves than you had available, good job!'.i18n;
        }
        if (match.level.levelId[1] == 6 &&
            match.level.levelId[0] + 1 == nextLevel[0] &&
            nextLevel[1] == 0) {
          content += '\n\n' +
              'Congratulations! You unlocked the %s numeral system. It is now available outside levels mode.'
                  .i18n
                  .fill([NumeralSystem.values[nextLevel[0]].name]);
        }
        titleColor = Color(playerColor[3]);
      } else {
        title = 'Too many moves'.i18n;
        content =
            'You made %s moves. You failed to solve the level in %s moves.'
                .i18n
                .fill([match.playerOne.moves, match.level.maxMoves]);
        titleColor = Color(playerColor[0]);
      }
      break;
    case GameMode.single:
      title = 'Congratulations'.i18n;
      content = 'You scored %s points in %s moves'
          .i18n
          .fill([match.playerOne.points, match.playerOne.moves]);
      break;
    case GameMode.multi:
      title = (winner != null)
          ? (winner.human ? 'You won'.i18n : '%s won'.i18n.fill([winner.name]))
          : 'You lost'.i18n;
      if (win) {
        content = byDeletion
            ? 'You won because you deleted your opponents.'.i18n
            : 'You won by doing more points than your opponents.'.i18n;
      } else {
        content = byDeletion
            ? 'You have been deleted.'.i18n
            : 'Your opponent won by doing more points than you.'.i18n;
      }
      titleColor = winner?.color ?? Colors.black38;
      break;
  }

  showDialog(
      context: gamePage.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GameDialog(
          willPop: false,
          title: title,
          titleColor: titleColor,
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(content),
                if (!match.isLevelMode)
                  for (var player in match.ranking)
                    PlayerInfo(
                      name: match.isMultiMode ? player.name : null,
                      points: player.points,
                      moves: player.moves,
                      color: player.color,
                      kills: player.kills,
                    ),
                Text('\n' + 'What do you want to do?'.i18n),
              ],
            ),
          ),
          actions: <Widget>[
            exitButton(context, gamePage),
            restartButton(gamePage,
                color: (win != null && (match.mode != GameMode.level || win))
                    ? null
                    : titleColor),
            if (match.isLevelMode && match.level.levelId[1] == 6 && win)
              DialogButton(
                text: 'Next Layer'.i18n.toUpperCase(),
                color: titleColor,
                onPressed: () {
                  if (match.level.levelId[0] != 0) pageIndex++;
                  pageController.jumpToPage(pageIndex);
                  Navigator.of(context).pop();
                  Navigator.pop(context);
                },
              ),
            if ((!match.isLevelMode && !match.loopRepeat) ||
                (match.isLevelMode && match.level.levelId[1] != 6 && win))
              DialogButton(
                color: titleColor ?? Color(playerColor[1]),
                text: match.isLevelMode
                    ? 'Next Level'.i18n.toUpperCase()
                    : 'New Match'.i18n.toUpperCase(),
                onPressed: () {
                  match.next();
                  gamePage.scaffoldKey.currentState.removeCurrentSnackBar();
                  gamePage.update();
                  Navigator.of(context).pop();
                },
              ),
          ],
        );
      });
}

import 'package:flutter/material.dart';

import 'package:zoysii/util/direction.dart';
import 'package:zoysii/util/local_data_controller.dart';

// Floating virtual gamepad used to move the pointer
class FloatingGamepad extends StatefulWidget {
  final Function onInput;
  FloatingGamepad(this.onInput);

  @override
  _FloatingGamepadState createState() => _FloatingGamepadState();
}

// Floating gamepad widget
class _FloatingGamepadState extends State<FloatingGamepad> {
  // Arrow button color
  final Color buttonColor = Colors.blueGrey[500];

  // Map direction-button icon
  final Map<Direction, dynamic> arrowIconMap = {
    Direction.up: const Icon(Icons.keyboard_arrow_up),
    Direction.down: const Icon(Icons.keyboard_arrow_down),
    Direction.left: const Icon(Icons.keyboard_arrow_left),
    Direction.right: const Icon(Icons.keyboard_arrow_right),
  };

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: gamepadOffset.dx,
      bottom: gamepadOffset.dy,
      child: Column(
        children: [
          Row(
            children: [
              separator(),
              arrowButton(Direction.up),
              separator(),
            ],
          ),
          Row(
            children: [
              arrowButton(Direction.left),
              if (settings.gamepadShape == 0)
                arrowButton(null)
              else
                arrowButton(Direction.down),
              arrowButton(Direction.right),
            ],
          ),
          if (settings.gamepadShape == 0)
            Row(
              children: [
                separator(),
                arrowButton(Direction.down),
                separator(),
              ],
            ),
        ],
      ),
    );
  }

  // Gamepad button widget
  Widget arrowButton(Direction direction) {
    return GestureDetector(
      // Edit widget position by moving it
      onPanUpdate: (details) {
        setState(() {
          gamepadOffset -= details.delta;
        });
      },
      onPanEnd: (_) {
        saveGamepadOffset();
      },
      child: direction != null
          ? IconButton(
              icon: arrowIconMap[direction],
              iconSize: settings.gamepadSize,
              color: buttonColor,
              onPressed: () {
                widget.onInput(direction);
              },
            )
          : Container(
              width: settings.gamepadSize,
              height: settings.gamepadSize,
              child: Icon(
                Icons.fiber_manual_record,
                size: settings.gamepadSize / 3,
              ),
            ),
    );
  }

  // White space in gamepad
  Widget separator() =>
      Container(width: settings.gamepadSize, height: settings.gamepadSize);
}

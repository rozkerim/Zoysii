import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/basic.dart';

// In game alert dialog
class GameDialog extends StatelessWidget {
  // Dialog title
  final String title;

  // Dialog content
  final Widget content;

  // Dialog buttons
  final List<Widget> actions;

  // True if dismissible
  final bool willPop;

  // Title color
  final Color titleColor;

  GameDialog({
    @required this.title,
    @required this.content,
    @required this.actions,
    this.willPop = true,
    this.titleColor,
  }) {
    match.isPause = true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        match.isPause = !willPop;
        return Future.value(willPop);
      },
      child: AlertDialog(
        elevation: 0,
        shape: const RoundedRectangleBorder(borderRadius: borderRadius),
        title: Container(
          height: 40,
          decoration: BoxDecoration(
            color: titleColor ?? Color(playerColor[1]),
            borderRadius: borderRadius,
          ),
          child: Center(
            child: Text(title, style: const TextStyle(color: Colors.white)),
          ),
        ),
        content: content,
        actions: actions,
      ),
    );
  }
}

// Player info container used in pause dialog
class PlayerInfo extends StatelessWidget {
  // Player name
  final String name;

  // Player points
  final int points;

  // PLayer moves
  final int moves;

  // Player color
  final Color color;

  // Player kills
  final int kills;

  PlayerInfo({
    this.name,
    this.points,
    this.kills = 0,
    @required this.moves,
    @required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
      decoration: BoxDecoration(
          border: Border(left: BorderSide(color: color, width: 4))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (name != null)
            Container(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  name + ' •' * kills,
                  style: TextStyle(
                      color: color, fontSize: 18, fontWeight: FontWeight.bold),
                )),
          if (points != null) Text('Points: %s'.i18n.fill([points])),
          Text('Moves: %s'.i18n.fill([moves])),
        ],
      ),
    );
  }
}

Widget cancelButton(BuildContext context, {Color color}) => DialogButton(
      text: 'Cancel'.i18n.toUpperCase(),
      color: color,
      onPressed: () {
        match.isPause = false;
        Navigator.of(context).pop();
      },
    );

Widget restartButton(GamePageState gamePage, {Color color}) => DialogButton(
      text: 'Restart'.i18n.toUpperCase(),
      color: color,
      onPressed: () {
        match.init();
        match.restartCount++;
        gamePage.update();
        Navigator.of(gamePage.context).pop();
      },
    );

Widget exitButton(BuildContext context, GamePageState gamePage, {Color color}) {
  return DialogButton(
    text: 'Exit'.i18n.toUpperCase(),
    color: color,
    onPressed: () {
      Navigator.pop(context);
      Navigator.pop(gamePage.context);
    },
  );
}

class DialogButton extends StatelessWidget {
  final String text;
  final Color color;
  final VoidCallback onPressed;

  DialogButton({
    @required this.text,
    this.color,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: color == null ? Theme.of(context).accentColor : null,
          ),
        ),
        decoration: color != null
            ? BoxDecoration(
                border: Border(bottom: BorderSide(color: color, width: 3)))
            : null,
      ),
    );
  }
}

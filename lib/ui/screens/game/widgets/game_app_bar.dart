import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/util/local_data_controller.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/dialogs.dart';
import 'package:zoysii/ui/screens/game/widgets/percent_indicator.dart';
import 'package:zoysii/ui/basic.dart';

class GameAppBar extends StatefulWidget implements PreferredSizeWidget {
  final GamePageState gamePageState;

  @override
  Size get preferredSize => const Size.fromHeight(100);

  GameAppBar(this.gamePageState);

  @override
  GameAppBarState createState() => GameAppBarState(gamePageState);
}

class GameAppBarState extends State<GameAppBar> {
  final GamePageState gamePageState;

  GameAppBarState(this.gamePageState) {
    _updateTitle();
  }

  // Title information displayed
  // 0: Remaining tiles / Level Name; 1: Moves; 2: Best result / Points away
  int titleType = match.isMultiMode ? 2 : 0;

  // App bar title
  String title;

  // App bar subtitle
  String subtitle;

  // Change title information displayed
  void nextTitle() {
    ++titleType;
    if (titleType > (match.isLevelMode ? 1 : 2)) titleType = 0;
    _updateTitle();
  }

  // Update title and subtitle text
  void _updateTitle() {
    switch (titleType) {
      case 0:
        if (match.isLevelMode) {
          title = match.level.name;
          subtitle = null;
        } else {
          title = match.board.remainingTiles.toString();
          subtitle = 'remaining tiles'.i18n;
        }
        break;
      case 1:
        title = match.playerOne.moves.toString();
        subtitle = 'moves'.i18n;
        break;
      case 2:
        title = match.resultToBeat.toString();
        subtitle = match.isMultiMode ? 'points away'.i18n : 'best result'.i18n;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    _updateTitle();
    return SafeArea(
      child: Column(children: [
        Container(
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: const Icon(Icons.close),
                tooltip: 'Exit'.i18n,
                onPressed: () {
                  exitDialog(gamePageState);
                },
              ),
              IconButton(
                icon: const Icon(Icons.repeat),
                tooltip: 'Repeat'.i18n,
                onPressed: () {
                  newMatchDialog(gamePageState);
                },
              ),
              Expanded(
                child: InkWell(
                    borderRadius: const BorderRadius.all(Radius.circular(40)),
                    child: Center(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(title, style: const TextStyle(fontSize: 32)),
                              if (subtitle != null)
                                Text(
                                  subtitle,
                                  style: const TextStyle(fontSize: 16),
                                )
                            ],
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      nextTitle();
                      setState(() {});
                    }),
              ),
              NumeralSystemButton(gamePageState),
              IconButton(
                icon: const Icon(Icons.pause),
                tooltip: 'Pause'.i18n,
                onPressed: () {
                  pauseDialog(gamePageState.context);
                },
              ),
            ],
          ),
        ),
        PercentIndicator(),
      ]),
    );
  }
}

class NumeralSystemButton extends StatelessWidget {
  final GamePageState gamePageState;
  final icon = const Icon(Icons.translate);

  NumeralSystemButton(this.gamePageState);

  @override
  Widget build(BuildContext context) {
    if (!match.isLevelMode) {
      return Visibility(
        visible: nextLevel[0] > 0,
        maintainSize: true,
        maintainState: true,
        maintainAnimation: true,
        child: PopupMenuButton<NumeralSystem>(
          icon: icon,
          tooltip: 'Numeral system'.i18n,
          shape: menuButtonShape,
          onSelected: (NumeralSystem selectedSystem) {
            match.selectedNumeralSystem = selectedSystem;
            gamePageState.update();
          },
          itemBuilder: (BuildContext context) {
            var popupEntries = <PopupMenuEntry<NumeralSystem>>[];
            for (var value in NumeralSystem.values) {
              if (value.index <= nextLevel[0]) {
                popupEntries.add(PopupMenuItem<NumeralSystem>(
                    value: value, child: Text(value.name)));
              }
            }
            return popupEntries;
          },
        ),
      );
    } else {
      return IconButton(
        icon: icon,
        tooltip: 'Numeral system'.i18n,
        onPressed: () {
          match.selectedNumeralSystem =
              (match.selectedNumeralSystem != NumeralSystem.Arabic)
                  ? NumeralSystem.Arabic
                  : NumeralSystem.values[match.level.levelId[0] + 1];
          gamePageState.update();
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 1),
              content: Text(
                'Numeral system'.i18n + ': ${match.selectedNumeralSystem.name}',
                textAlign: TextAlign.center,
              ),
            ),
          );
        },
      );
    }
  }
}

import 'package:flutter/material.dart';

import 'package:zoysii/models/numeral_system.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/util/notations.dart';

class GameGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      padding: const EdgeInsets.symmetric(vertical: 10),
      physics: const NeverScrollableScrollPhysics(),
      crossAxisCount: match.board.width,
      children: _gridComposer(context),
    );
  }

  // Generate game grid and return list of tile widgets
  List<Widget> _gridComposer(BuildContext context) {
    // Map<position, color_value>
    // It only contains players' position as keys
    var colorMap = <int, int>{};
    var bottomBorder = <int, int>{};
    match.players.asMap().forEach((index, player) {
      if (player.alive) {
        colorMap[player.position] =
            (colorMap[player.position] ?? 0) + player.color.value;
        if (match.canMove(player)) {
          bottomBorder[player.position] =
              (bottomBorder[player.position] ?? 0) + player.color.value;
        }
      }
    });

    return List.generate(match.board.grid.length, (index) {
      return Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 2),
          decoration:
              (bottomBorder[index] == null || match.board.grid[index] == 0)
                  ? null
                  : BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Color(bottomBorder[index]), width: 3))),
          child: FittedBox(
            child: Text(
              match.board.grid[index].toNotation(match.selectedNumeralSystem,
                  isPlayer: colorMap[index] != null),
              style: TextStyle(
                fontSize: colorMap[index] == null ? 26 : 32,
                color: Color(
                    colorMap[index] ?? Theme.of(context).accentColor.value),
                fontFamily: match.selectedNumeralSystem.fontFamily,
              ),
            ),
          ),
        ),
      );
    });
  }
}

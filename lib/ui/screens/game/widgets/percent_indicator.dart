import 'dart:math';
import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';

// Calculate percentage values and return _PercentIndicator widget
Widget PercentIndicator() {
  String text;
  List<int> values;
  switch (match.mode) {
    case GameMode.level:
      text = 'Remaining moves: %s'
          .i18n
          .fill([max(0, match.level.maxMoves - match.playerOne.moves)]);
      values = [
        match.level.maxMoves - match.playerOne.moves,
        match.playerOne.moves
      ];
      break;
    case GameMode.single:
      text = 'Points: %s'.i18n.fill([match.playerOne.points]) +
          (match.isMultiMode ? ' - ${match.playerTwo.points}' : '');
      values = [
        max(1, match.playerOne.points),
        match.resultToBeat - match.playerOne.points
      ];
      break;
    case GameMode.multi:
      values = [
        max(1, match.playerOne.points),
        max(1, match.playerTwo.points),
        if (match.players.length >= 3) max(1, match.playerThree.points),
        if (match.players.length >= 4) max(1, match.playerFour.points),
      ];
      break;
  }
  return _PercentIndicator(text: text, values: values, colors: [
    match.playerOne.color,
    match.playerTwo?.color,
    match.playerThree?.color,
    match.playerFour?.color,
  ]);
}

// Percent indicator widget
class _PercentIndicator extends StatelessWidget {
  // Percentage values
  final List<int> values;

  // Bar colors
  final List<Color> colors;

  // Text displayed on the percent bar
  final String text;

  _PercentIndicator({@required this.values, @required this.colors, this.text})
      : assert(values.length > 1 && colors.length > 1);

  final double height = 25;

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Row(
        children: <Widget>[
          for (var i = 0; i < values.length; i++)
            Expanded(
              flex: values[i],
              child: Container(
                height: height,
                decoration: BoxDecoration(
                  color: colors.length > i && colors[i] != null
                      ? colors[i]
                      : Color(playerColor[1]),
                  border: match.players.length > i &&
                          match.isMultiMode &&
                          match.gameSpeed == 0 &&
                          match.canMove(match.players[i])
                      ? Border(
                          bottom: BorderSide(
                              color: Theme.of(context).accentColor, width: 3))
                      : null,
                ),
                child: match.players.length > i &&
                        match.isMultiMode &&
                        match.ranking.first == match.players[i]
                    ? const Icon(Icons.star, color: Colors.white)
                    : null,
              ),
            ),
        ],
      ),
      Center(
        child: Text(
          text ?? '',
          style: const TextStyle(fontSize: 18, color: Colors.white),
        ),
      ),
    ]);
  }
}

import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/models/game_mode.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/home/widgets/bottom_row.dart';
import 'package:zoysii/ui/screens/home/widgets/page_button.dart';
import 'package:zoysii/ui/screens/levels/levels_page.dart';
import 'package:zoysii/ui/screens/multiplayer/multiplayer_page.dart';

class HomePage extends StatelessWidget {
  List<Map<String, dynamic>> pageList() => [
        {'title': 'Quick match'.i18n, 'goto': () => GamePage(GameMode.single)},
        {'title': 'Multiplayer'.i18n, 'goto': () => MultiPlayerPage()},
        {'title': 'Levels'.i18n, 'goto': () => LevelsPage()},
      ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: Theme.of(context).brightness == Brightness.light
                ? appGradient
                : darkAppGradient),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Expanded(
              flex: 5,
              child: Center(
                child: Text(
                  'ZOYSII',
                  style: TextStyle(
                    letterSpacing: 14,
                    color: Colors.white,
                    fontSize: 60,
                    fontFamily: 'NotoSansKhmer',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            for (var page in pageList())
              PageButton(
                title: page['title'],
                onPressed: () {
                  Navigator.push(context, FadeRoute(page['goto']()));
                },
              ),
            const Spacer(),
            bottomRow(),
          ],
        ),
      ),
    );
  }
}

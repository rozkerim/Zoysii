import 'package:flutter/material.dart';

import 'package:zoysii/i18n/strings.i18n.dart';
import 'package:zoysii/util/local_data_controller.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/widgets/play_button.dart';

class MultiPlayerPage extends StatefulWidget {
  @override
  _MultiPlayerPageState createState() => _MultiPlayerPageState();
}

class _MultiPlayerPageState extends State<MultiPlayerPage> {
  var rangeValues = RangeValues(
      settings.minBoardInt.toDouble(), settings.maxBoardInt.toDouble());

  List<String> speedStrings() => [
        'Turn-based'.i18n,
        'Low'.i18n,
        'Medium'.i18n,
        'High'.i18n,
        'Very high'.i18n,
      ];
  final speedValues = <int>[0, 1500, 1200, 900, 600];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Multiplayer'.i18n.toUpperCase()),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 70),
        children: [slider(), const Divider(), ...playerTileList()],
      ),
      floatingActionButton: PlayButton(
        playerCount: settings.playersMulti,
        gameSpeed: speedValues[settings.speedMulti],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget slider() {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: ExpansionTile(
        title: Text('Speed'.i18n, style: const TextStyle(fontSize: 20)),
        subtitle: Text(
          'Game speed'.i18n,
          style: TextStyle(color: Theme.of(context).textTheme.caption.color),
        ),
        trailing: Text(
          speedStrings()[settings.speedMulti],
          style: const TextStyle(fontSize: 16),
        ),
        initiallyExpanded: false,
        children: <Widget>[
          Slider(
            value: settings.speedMulti.toDouble(),
            min: 0,
            max: 4,
            onChanged: (double newValue) {
              setState(() {
                settings.speedMulti = newValue.round();
              });
              saveSettings();
            },
          ),
        ],
      ),
    );
  }

  List<Widget> playerTileList() {
    var _playerList = <Widget>[];
    for (var index = 0; index < settings.playersMulti; index++) {
      _playerList.add(Column(
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Color(playerColor[index]),
              child: Text(
                (index + 1).toString(),
                style: const TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
            title: Text(
              index != 0 ? 'CPU $index' : 'You'.i18n,
              style: const TextStyle(fontSize: 18),
            ),
            trailing: (index + 1 == settings.playersMulti && index > 1)
                ? IconButton(
                    icon: const Icon(Icons.clear),
                    tooltip: 'Remove'.i18n,
                    onPressed: () {
                      setState(() {
                        settings.playersMulti--;
                      });
                      saveSettings();
                    },
                  )
                : null,
          ),
          const Divider(color: Colors.transparent),
        ],
      ));
    }
    if (settings.playersMulti < 4) {
      _playerList.addAll([
        const Divider(),
        FlatButton(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [const Icon(Icons.add), Text('Add new player'.i18n)],
          ),
          onPressed: () {
            setState(() {
              settings.playersMulti++;
            });
            saveSettings();
          },
        ),
        const Divider(),
      ]);
    }
    return _playerList;
  }
}
